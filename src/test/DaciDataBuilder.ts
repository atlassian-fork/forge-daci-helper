import adfUtil, {AdfNode, TableCellPosition} from '../AdfUtil';
import {
  Impact, ProConType,
  Status
} from '../types';
import pbc from '../PBC';

export default class DaciDataBuilder {

  status: undefined | string | Status = Status.NotStarted;
  statusColor: string = 'neutral';
  impact: undefined | string | Impact = Impact.Low;
  impactColor: string = 'green';
  driver: undefined | string = 'Steve Wozniak';
  dueDate: undefined | Date = undefined;
  approver: undefined | string = 'Bart Simpson';
  informed: undefined | string = 'Bill Gates';
  contributors: undefined | string = 'Bill Gates';
  outcome: undefined | string = 'Sitting on the fence';
  backgroundSectionText: undefined | string = 'Not much of a background here';
  relevantDataSectionText: undefined | string = 'No relevant data';
  optionDescriptionTexts: string[] = ['A description for option 1', 'A description for option 2'];
  optionProsAndConsCounts: number[] = [4, 4];

  constructor() {
  }

  setStatus = (status: undefined | string | Status): DaciDataBuilder => {
    this.status = status;
    return this;
  };

  setStatusColor = (statusColor: string): DaciDataBuilder => {
    this.statusColor = statusColor;
    return this;
  };

  setImpact = (impact: undefined | string | Impact): DaciDataBuilder => {
    this.impact = impact;
    return this;
  };

  setImpactColor = (impactColor: string): DaciDataBuilder => {
    this.impactColor = impactColor;
    return this;
  };

  setDriver = (driver: undefined | string): DaciDataBuilder => {
    this.driver = driver;
    return this;
  };

  setDueDate = (dueDate: undefined | Date): DaciDataBuilder => {
    this.dueDate = dueDate;
    return this;
  };

  setApprover = (approver: undefined | string): DaciDataBuilder => {
    this.approver = approver;
    return this;
  };

  setInformed = (informed: undefined | string): DaciDataBuilder => {
    this.informed = informed;
    return this;
  };

  setContributors = (contributors: undefined | string): DaciDataBuilder => {
    this.contributors = contributors;
    return this;
  };

  setOutcome = (outcome: undefined | string): DaciDataBuilder => {
    this.outcome = outcome;
    return this;
  };

  setBackgroundSectionText = (backgroundSectionText: undefined | string): DaciDataBuilder => {
    this.backgroundSectionText = backgroundSectionText;
    return this;
  };

  setRelevantDataSectionText = (relevantDataSectionText: undefined | string): DaciDataBuilder => {
    this.relevantDataSectionText = relevantDataSectionText;
    return this;
  };

  setOptionDescriptionTexts = (texts: string[]): DaciDataBuilder => {
    pbc.assertTruthy(texts, 'texts');
    pbc.assertEqual(texts.length, 2, 'texts.length');
    this.optionDescriptionTexts = texts;
    return this;
  };

  setOptionProsAndConsCounts = (counts: number[]): DaciDataBuilder => {
    pbc.assertTruthy(counts, 'counts');
    pbc.assertEqual(counts.length, 2, 'counts.length');
    this.optionProsAndConsCounts = counts;
    return this;
  };

  build = (): AdfNode => {
    let daci = this.daciTemplate;
    // console.log('daci:', daci);
    daci = this.replaceField(daci, 'status', this.status);
    daci = this.replaceField(daci, 'statusColor', this.statusColor);
    daci = this.replaceField(daci, 'impact', this.impact);
    daci = this.replaceField(daci, 'impactColor', this.impactColor);
    daci = this.replaceFieldWithMention(daci, 'driver', this.driver);
    daci = this.replaceField(daci, 'dueDate', this.dueDate ? this.dueDate.getTime() : '');
    daci = this.replaceFieldWithMention(daci, 'approver', this.approver);
    daci = this.replaceFieldWithMention(daci, 'informed', this.informed);
    daci = this.replaceFieldWithMention(daci, 'contributors', this.contributors);
    daci = this.replaceField(daci, 'outcome', this.outcome);
    daci = this.replaceField(daci, 'backgroundSectionText', this.backgroundSectionText);
    daci = this.replaceField(daci, 'relevantDataSectionText', this.relevantDataSectionText);
    for (let index = 0; index < this.optionDescriptionTexts.length; index++) {
      const optionNumber = index + 1;
      const field = `option-${optionNumber}-description`;
      const optionDescription = this.optionDescriptionTexts[index];
      daci = this.replaceField(daci, field, optionDescription);
    }
    for (let index = 0; index < this.optionProsAndConsCounts.length; index++) {
      const optionNumber = index + 1;
      const field = `option-${optionNumber}-pros-and-cons`;
      const count = this.optionProsAndConsCounts[index];
      const prosAndCons = this.buildProsAndCons(count);
      const prosAndConsJson = JSON.stringify(prosAndCons, null, 2);
      daci = this.replaceField(daci, field, prosAndConsJson);
    }
    // console.log('daci:', daci);
    return JSON.parse(daci);
  };

  buildProsAndCons = (count) => {
    const paragraph = this.buildParagraphNode();
    const emoji = this.buildProConEmojiNode(ProConType.Warning);
    for (let i = 0; i < count; i++) {
      paragraph.content.push(emoji);
    }
    return paragraph;
  };

  buildParagraphNode = () => {
    return {
      'type': 'paragraph',
      'content': []
    }
  };

  buildProConEmojiNode = (proConType: ProConType) => {
    let attrs = {};
    if (proConType === ProConType.Minus) {
      attrs = {
        id: 'atlassian-minus',
        text: ':minus:',
        shortName: ':minus:'
      }
    } else if (proConType === ProConType.Plus) {
      attrs = {
        id: 'atlassian-plus',
        text: ':plus:',
        shortName: ':plus:'
      }
    } else if (proConType === ProConType.Warning) {
      attrs = {
        id: 'atlassian-warning',
        text: ':warning:',
        shortName: ':warning:'
      }
    } else if (proConType === ProConType.Question) {
      attrs = {
        id: 'atlassian-question_mark',
        text: ':question_mark:',
        shortName: ':question_mark:'
      }
    }
    return {
      type: 'emoji',
      attrs: attrs
    };
  };

  replaceField = (daci, field, replacementValue) => {
    const searchText = '[[' + field + '-value]]';
    // const index = daci.indexOf(searchText);
    // console.log('Index:', index);
    const replacementText = replacementValue ? replacementValue : '';
    return daci.replace(searchText, replacementText);
  };

  replaceFieldWithMention = (daci, field, mentionName) => {
    const searchText = '[[' + field + '-value]]';
    const mentionAdf = mentionName ? `{
                          "type": "mention",
                          "attrs": {
                            "id": "557057:7065aba1-f766-4f8f-a164-1111111",
                            "text": "@${mentionName}"
                          }
                        }` : '';
    return daci.replace(searchText, mentionAdf);
  };

  daciTemplate: string = `{
  "type": "doc",
  "content": [
    {
      "type": "extension",
      "attrs": {
        "layout": "default",
        "extensionType": "com.atlassian.ecosystem",
        "extensionKey": "xen:macro",
        "text": "DACI Helper (Development) - Forge Extension",
        "parameters": {
          "localId": "e50a4119-d826-46ac-8100-bfbcbb8ecc62",
          "extensionId": "ari:cloud:ecosystem::extension/0588117a-182a-470b-8e4e-6359ff30ce78/f4f2a55d-e528-4dda-b733-7d1eee0e069d/static/daci-helper",
          "extensionTitle": "DACI Helper (Development)"
        }
      }
    },
    {
      "type": "panel",
      "attrs": {
        "panelType": "info"
      },
      "content": [
        {
          "type": "paragraph",
          "content": [
            {
              "text": "Add your comments directly to the page. Include links to any relevant research, data, or feedback.",
              "type": "text"
            }
          ]
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "type": "placeholder",
          "attrs": {
            "text": "Summarize this decision in the table below. Type /date to quickly add the due date and @mention the driver, approver, contributors, and informed to keep everyone on the same page."
          }
        },
        {
          "text": "\\n\\n",
          "type": "text"
        }
      ]
    },
    {
      "type": "bodiedExtension",
      "attrs": {
        "layout": "default",
        "extensionType": "com.atlassian.confluence.macro.core",
        "extensionKey": "details",
        "text": " | label",
        "parameters": {
          "macroParams": {
            "label": {
              "value": ""
            }
          },
          "macroMetadata": {
            "macroId": {
              "value": "18c31c3d-410f-454c-8214-b262cefcb47d"
            },
            "schemaVersion": {
              "value": "1"
            },
            "title": "Page Properties"
          }
        }
      },
      "content": [
        {
          "type": "table",
          "attrs": {
            "layout": "default"
          },
          "content": [
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Status",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "type": "status",
                          "attrs": {
                            "color": "[[statusColor-value]]",
                            "style": "bold",
                            "text": "[[status-value]]",
                            "localId": "b5050e33-7899-43be-82f9-bcfbb845c5b9"
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Impact",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "type": "status",
                          "attrs": {
                            "color": "[[impactColor-value]]",
                            "style": "bold",
                            "text": "[[impact-value]]",
                            "localId": "5ffa8f7f-ade4-4fe9-b31f-e9bf8ea61320"
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Driver",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        [[driver-value]]
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Approver",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        [[approver-value]]
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Contributors",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        [[contributors-value]]
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Informed",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        [[informed-value]]
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Due date",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "type": "date",
                          "attrs": {
                            "timestamp": "[[dueDate-value]]"
                          }
                        }
                      ]
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableRow",
              "content": [
                {
                  "type": "tableHeader",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      162
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "text": "Outcome",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                },
                {
                  "type": "tableCell",
                  "attrs": {
                    "colspan": 1,
                    "rowspan": 1,
                    "colwidth": [
                      597
                    ]
                  },
                  "content": [
                    {
                      "type": "paragraph",
                      "content": [
                        {
                          "type": "placeholder",
                          "attrs": {
                            "text": "What did you decide?"
                          }
                        },
                        {
                          "text": "[[outcome-value]]",
                          "type": "text"
                        }
                      ]
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "type": "heading",
      "attrs": {
        "level": 2
      },
      "content": [
        {
          "text": "Background",
          "type": "text"
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "type": "placeholder",
          "attrs": {
            "text": "Provide context on the decision the team needs to make. Include links to relevant research, pages, and related decisions, as well as information on constraints or challenges that may impact the outcome."
          }
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "text": "[[backgroundSectionText-value]]",
          "type": "text"
        }
      ]
    },
    {
      "type": "heading",
      "attrs": {
        "level": 2
      },
      "content": [
        {
          "text": "Relevant data",
          "type": "text"
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "type": "placeholder",
          "attrs": {
            "text": "Add any data or feedback the team should consider when making this decision"
          }
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "text": "[[relevantDataSectionText-value]]",
          "type": "text"
        }
      ]
    },
    {
      "type": "heading",
      "attrs": {
        "level": 2
      },
      "content": [
        {
          "text": "Options considered",
          "type": "text"
        }
      ]
    },
    {
      "type": "table",
      "attrs": {
        "layout": "default"
      },
      "content": [
        {
          "type": "tableRow",
          "content": [
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  334
                ]
              },
              "content": [
                {
                  "type": "paragraph"
                }
              ]
            },
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  209
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "Option 1:",
                      "type": "text"
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  216
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "Option 2:",
                      "type": "text"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "tableRow",
          "content": [
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  334
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "Description",
                      "type": "text"
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  209
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "[[option-1-description-value]]",
                      "type": "text"
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  216
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "[[option-2-description-value]]",
                      "type": "text"
                    }
                  ]
                }
              ]
            }
          ]
        },
        {
          "type": "tableRow",
          "content": [
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  334
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "Pros and cons",
                      "type": "text"
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  209
                ]
              },
              "content": [
                [[option-1-pros-and-cons-value]]
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  216
                ]
              },
              "content": [
                [[option-2-pros-and-cons-value]]
              ]
            }
          ]
        },
        {
          "type": "tableRow",
          "content": [
            {
              "type": "tableHeader",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  334
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "text": "Estimated cost",
                      "type": "text"
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  209
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "type": "status",
                      "attrs": {
                        "color": "red",
                        "style": "bold",
                        "text": "LARGE",
                        "localId": "31640400-f619-4d91-b85b-b58f5daa7845"
                      }
                    }
                  ]
                }
              ]
            },
            {
              "type": "tableCell",
              "attrs": {
                "colspan": 1,
                "rowspan": 1,
                "colwidth": [
                  216
                ]
              },
              "content": [
                {
                  "type": "paragraph",
                  "content": [
                    {
                      "type": "status",
                      "attrs": {
                        "color": "yellow",
                        "style": "bold",
                        "text": "MEDIUM",
                        "localId": "9062c68d-9be5-42c8-b10f-b665aab3e35a"
                      }
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      "type": "heading",
      "attrs": {
        "level": 2
      },
      "content": [
        {
          "text": "Action items",
          "type": "text"
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "type": "placeholder",
          "attrs": {
            "text": "Add action items to close the loop on open questions or concerns"
          }
        }
      ]
    },
    {
      "type": "taskList",
      "attrs": {
        "localId": ""
      },
      "content": [
        {
          "type": "taskItem",
          "attrs": {
            "state": "TODO",
            "localId": "1"
          },
          "content": [
            {
              "type": "placeholder",
              "attrs": {
                "text": "Type your task here. Use \\"@\\" to assign a user and \\"//\\" to select a due date."
              }
            }
          ]
        },
        {
          "type": "taskItem",
          "attrs": {
            "state": "TODO",
            "localId": "2"
          },
          "content": [
            {
              "text": "Test action item",
              "type": "text"
            }
          ]
        }
      ]
    },
    {
      "type": "heading",
      "attrs": {
        "level": 2
      },
      "content": [
        {
          "text": "Outcome",
          "type": "text"
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "type": "placeholder",
          "attrs": {
            "text": "Summarize the outcome below"
          }
        }
      ]
    },
    {
      "type": "paragraph",
      "content": [
        {
          "text": "Test outcome",
          "type": "text"
        }
      ]
    }
  ],
  "version": 1
}`;

}

// export default new DaciData();