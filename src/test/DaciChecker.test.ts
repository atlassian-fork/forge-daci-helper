// import daciChecker from '../DaciChecker';
import DaciChecker from '../DaciChecker';
import DaciDataBuilder from './DaciDataBuilder';
import adfUtil, {AdfNode, TableCellPosition} from '../AdfUtil';
import {
  allowedStatuses,
  Impact,
  Status,
  ValidationResultType,
  ValidationResult, DaciValidation
} from '../types';
import validationUtil from '../ValidationUtil';

test('valid-daci', async () => {
  const providedStatus = Status.Obsolete;
  const providedStatusColor = 'purple';
  const providedImpact = Impact.High;
  const providedImpactColor = 'red';
  const providedDriver = 'Steve Wozniak';
  const providedApprover = 'Foo';
  const providedInformed = 'Bar';
  const providedContributors = 'Ada Lovelace';
  const providedOutcome = 'This is no longer required';
  const providedBackground = 'my background';
  const providedRelevantData = 'my relevant data';
  const providedOptionDescription1 = 'A valid description for option 1';
  const providedOptionDescription2 = 'A valid description for option 2';
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setStatus(providedStatus)
    .setStatusColor(providedStatusColor)
    .setImpact(providedImpact)
    .setImpactColor(providedImpactColor)
    .setDriver(providedDriver)
    .setDueDate(providedDueDate)
    .setApprover(providedApprover)
    .setInformed(providedInformed)
    .setContributors(providedContributors)
    .setOutcome(providedOutcome)
    .setBackgroundSectionText(providedBackground)
    .setRelevantDataSectionText(providedRelevantData)
    .setOptionDescriptionTexts([providedOptionDescription1, providedOptionDescription2])
    .setOptionProsAndConsCounts([4, 4])
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setMinimumBackgroundLength(providedBackground.length)
    .setMinimumRelevantDataLength(providedRelevantData.length)
    .setMinimumBackgroundAndRelevantDataLength(providedBackground.length + providedRelevantData.length)
    .setMinimumOptionDescriptionLength(Math.min(providedOptionDescription1.length, providedOptionDescription2.length))
    .setMinimumProsConsPerOption(2)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const errorCount = validationUtil.countErrors(daciValidation.results);
  expect(errorCount).toBe(0);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
  expect(daciValidation.status).toBe(providedStatus);
  expect(daciValidation.impact).toBe(providedImpact);
  expect(daciValidation.driver).toBe(providedDriver);
  expect(daciValidation.informed).toBe(providedInformed);
  expect(daciValidation.dueDate.getTime()).toBe(providedDueDate.getTime());
});

test('invalid-daci-missing-fields', async () => {
  const providedStatus = 'Invalid status';
  const providedImpact = 'Invalid impact';
  const providedDriver = '';
  const providedApprover = '';
  const providedInformed = '';
  const providedContributors = '';
  const providedOutcome = '';
  const providedBackground = '';
  const providedRelevantData = '';
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setStatus(providedStatus)
    .setImpact(providedImpact)
    .setDriver(providedDriver)
    .setDueDate(providedDueDate)
    .setApprover(providedApprover)
    .setInformed(providedInformed)
    .setContributors(providedContributors)
    .setOutcome(providedOutcome)
    .setBackgroundSectionText(providedBackground)
    .setRelevantDataSectionText(providedRelevantData)
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(providedBackground.length + 1)
    .setMinimumRelevantDataLength(providedRelevantData.length + 1)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(9);
  expect(daciValidation.status).toBe(undefined);
  expect(daciValidation.impact).toBe(providedImpact);
  expect(daciValidation.driver).toBe(providedDriver);
  expect(daciValidation.informed).toBe(providedInformed);
  expect(daciValidation.dueDate.getTime()).toBe(providedDueDate.getTime());
  expect(daciValidation.backgroundText).toBe(providedBackground);
});

test('invalid-daci-not-enough-info', async () => {
  const providedStatus = Status.Obsolete;
  const providedStatusColor = 'purple';
  const providedImpact = Impact.High;
  const providedImpactColor = 'red';
  const providedDriver = 'Steve Wozniak';
  const providedApprover = 'Foo';
  const providedInformed = 'Bar';
  const providedContributors = 'Ada Lovelace';
  const providedOutcome = 'This is no longer required';
  const providedBackground = 'my background';
  const providedRelevantData = 'my relevant data';
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setStatus(providedStatus)
    .setStatusColor(providedStatusColor)
    .setImpact(providedImpact)
    .setImpactColor(providedImpactColor)
    .setDriver(providedDriver)
    .setDueDate(providedDueDate)
    .setApprover(providedApprover)
    .setInformed(providedInformed)
    .setContributors(providedContributors)
    .setOutcome(providedOutcome)
    .setBackgroundSectionText(providedBackground)
    .setRelevantDataSectionText(providedRelevantData)
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(providedBackground.length + 1)
    .setMinimumRelevantDataLength(providedRelevantData.length + 1)
    .setMinimumBackgroundAndRelevantDataLength(providedBackground.length + providedRelevantData.length + 1)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(3);
  expect(daciValidation.status).toBe(providedStatus);
  expect(daciValidation.impact).toBe(providedImpact);
  expect(daciValidation.driver).toBe(providedDriver);
  expect(daciValidation.informed).toBe(providedInformed);
  expect(daciValidation.dueDate.getTime()).toBe(providedDueDate.getTime());
});

test('invalid-daci-overdue', async () => {
  const timestampOfDueDate = 1;
  const timestampOfCheck = timestampOfDueDate + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.NotStarted)
    .setOutcome('')
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
  expect(daciValidation.dueDate.getTime()).toBe(providedDueDate.getTime());
});

test('invalid-daci-decision-due-too-far-away', async () => {
  const timestampOfCheck = 0;
  const timestampOfDueDate = timestampOfCheck + 100 * 24 * 60 * 60 * 1000;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.InProgress)
    .setStatusColor('blue')
    .setOutcome('')
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
  expect(daciValidation.dueDate.getTime()).toBe(providedDueDate.getTime());
});

test('invalid-daci-missing-outcome', async () => {
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.Done)
    .setStatusColor('green')
    .setOutcome('')
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
});

test('invalid-daci-missing-outcome-when-obsolete', async () => {
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.Obsolete)
    .setStatusColor('purple')
    .setOutcome('')
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
});

test('invalid-daci-premature-outcome', async () => {
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.InProgress)
    .setStatusColor('blue')
    .setOutcome('this is premature')
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(1);
});

test('invalid-daci-different-numbers-of-pros-and-cons-per-option', async () => {
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.NotStarted)
    .setStatusColor('neutral')
    .setOutcome('')
    .setOptionProsAndConsCounts([1, 2])
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Error)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .setMinimumProsConsPerOption(1)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(2);
});

test('invalid-daci-option-with-too-few-pros-cons', async () => {
  const timestampOfCheck = 1;
  const timestampOfDueDate = timestampOfCheck + 1;
  const dateOfCheck = new Date(timestampOfCheck);
  const providedDueDate = new Date(timestampOfDueDate);
  const adf: AdfNode = new DaciDataBuilder()
    .setDueDate(providedDueDate)
    .setStatus(Status.NotStarted)
    .setStatusColor('neutral')
    .setOutcome('')
    .setOptionProsAndConsCounts([1, 1])
    .build();
  const daciValidation = new DaciChecker(adf)
    .setDateOfCheck(dateOfCheck)
    .setProblemSeverityOfSingleProsConsRow(ValidationResultType.Pass)
    .setMinimumBackgroundLength(0)
    .setMinimumRelevantDataLength(0)
    .setMinimumBackgroundAndRelevantDataLength(0)
    .setMinimumOptionDescriptionLength(0)
    .setMinimumProsConsPerOption(444)
    .check();
  // validationUtil.logResults(daciValidation.results);
  const problemCount = validationUtil.countProblems(daciValidation.results);
  expect(problemCount).toBe(2);
});
