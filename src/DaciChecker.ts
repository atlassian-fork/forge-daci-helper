import adfUtil, {AdfNode, LocatedNode, TableCellPosition} from './AdfUtil';
import {
  allowedImpacts,
  allowedStatuses,
  Status,
  ValidationResultType,
  ValidationResult, DaciValidation, Impact, ProConType
} from './types';
import util from './Util';

export default class DaciChecker {

  parameters = {
    adf: undefined,
    dateOfCheck: new Date(),
    problemSeverityOfSingleProsConsRow: ValidationResultType.Warning,
    minimumBackgroundLength: 30,
    minimumRelevantDataLength: 30,
    minimumBackgroundPlusRelevantDataLength: 800,
    minimumOptionDescriptionLength: 20,
    minimumOptionsCount: 2,
    minimumProsConsPerOption: 2,
    maximumDaysUntilLowImpactDecisionDue: 14,
    maximumDaysUntilMediumImpactDecisionDue: 28,
    maximumDaysUntilHighImpactDecisionDue: 56,
    linkTextLengthContribution: 500
  };
  daciData = {
    status: '',
    impact: '',
    driver: '',
    approver: '',
    informed: '',
    contributors: '',
    outcome: '',
    dueDate: undefined,
    overdue: false,
    millisecondsUntilDue: undefined,
    backgroundText: '',
    backgroundLinkCount: 0,
    relevantDataText: '',
    relevantDataLinkCount: 0,
    prosAndConsRowCount: 0,
    optionNumbersToData: {}
  };
  results: ValidationResult[];

  constructor(adf) {
    this.parameters.adf = adf;
  }

  setProblemSeverityOfSingleProsConsRow = (problemSeverityOfSingleProsConsRow: ValidationResultType) => {
    this.parameters.problemSeverityOfSingleProsConsRow = problemSeverityOfSingleProsConsRow;
    return this;
  };

  setDateOfCheck = (dateOfCheck: Date) => {
    this.parameters.dateOfCheck = dateOfCheck;
    return this;
  };

  setMinimumBackgroundLength = (minimumBackgroundLength: number) => {
    this.parameters.minimumBackgroundLength = minimumBackgroundLength;
    return this;
  };

  setMinimumRelevantDataLength = (minimumRelevantDataLength: number) => {
    this.parameters.minimumRelevantDataLength = minimumRelevantDataLength;
    return this;
  };

  setMinimumBackgroundAndRelevantDataLength = (minimumBackgroundPlusRelevantDataLength: number) => {
    this.parameters.minimumBackgroundPlusRelevantDataLength = minimumBackgroundPlusRelevantDataLength;
    return this;
  };

  setMinimumOptionDescriptionLength = (minimumOptionDescriptionLength: number) => {
    this.parameters.minimumOptionDescriptionLength = minimumOptionDescriptionLength;
    return this;
  };

  setMinimumProsConsPerOption = (minimumProsConsPerOption: number) => {
    this.parameters.minimumProsConsPerOption = minimumProsConsPerOption;
    return this;
  };

  check = (): DaciValidation => {
    this.results = [];
    this.checkSummaryTableInfo();
    this.checkText();
    this.checkOptionsTable();
    const daciValidation: DaciValidation = {
      results: this.results,
      status: this.daciData.status,
      impact: this.daciData.impact,
      driver: this.daciData.driver,
      informed: this.daciData.informed,
      contributors: this.daciData.contributors,
      dueDate: this.daciData.dueDate,
      overdue: this.daciData.overdue,
      millisecondsUntilDue: this.daciData.millisecondsUntilDue,
      backgroundText: this.daciData.backgroundText,
      backgroundLinkCount: this.daciData.backgroundLinkCount,
      relevantDataText: this.daciData.relevantDataText,
      relevantDataLinkCount: this.daciData.relevantDataLinkCount
    };
    return daciValidation;
  };

  addPass = (message: string) => {
    this.addValidationResult(message, ValidationResultType.Pass);
  };

  addWarning = (message: string) => {
    this.addValidationResult(message, ValidationResultType.Warning);
  };

  addError = (message: string) => {
    this.addValidationResult(message, ValidationResultType.Error);
  };

  addValidationResult = (message: string, validationResultType: ValidationResultType) => {
    const result: ValidationResult = {
      type: validationResultType,
      message: message
    };
    this.results.push(result);
  };

  validateStatusNodeFormat = (statusNode: AdfNode, daciNodeType: string, textLowersToColors: any) => {
    const text = adfUtil.getStatusText(statusNode);
    const requiredColor = textLowersToColors[text.toLowerCase().trim()];
    const actualColor = adfUtil.getStatusColor(statusNode);
    if (requiredColor !== undefined && actualColor !== undefined && actualColor !== requiredColor) {
      this.addError(`Detected ${daciNodeType} node containing text "${text.toUpperCase()}", but with a color of ${actualColor} when it should be ${requiredColor}. This should be fixed to avoid misinterpretation by readers.`);
    }
  };

  checkStatusInCell = (valueCell: AdfNode, rowHeaderText: string, checker: Function) => {
    const statusNodes = adfUtil.findStatusNodes(valueCell);
    if (statusNodes && statusNodes.length) {
      if (statusNodes.length === 1) {
        const statusNode = statusNodes[0];
        checker(statusNode, rowHeaderText);
      } else {
        this.addError(`Too many ${rowHeaderText} status nodes were detected!`);
      }
    } else {
      this.addError(`Could not find a ${rowHeaderText} status node!`);
    }
  };

  checkMentionsInCell = (valueCell: AdfNode, rowHeaderText: string, minMentions: number, maxMentions: number, checker: Function) => {
    const mentionNodes = adfUtil.findMentionNodes(valueCell);
    if (mentionNodes.length < minMentions) {
      if (minMentions === 1) {
        if (maxMentions === 1) {
          this.addError(`Expected exactly 1 ${rowHeaderText} to be mentioned!`);
        } else {
          this.addError(`Expected at least 1 ${rowHeaderText} to be mentioned!`);
        }
      } else {
        this.addError(`Expected at least ${minMentions} ${rowHeaderText}s to be mentioned!`);
      }
    } else {
      if (mentionNodes.length > maxMentions) {
        this.addError(`There are too many ${rowHeaderText}s mentioned (Max = ${maxMentions})!`);
      } else {
        checker(mentionNodes, rowHeaderText);
      }
    }
  };

  checkDatesInCell = (valueCell: AdfNode, rowHeaderText: string, minDates: number, maxDates: number, checker: Function) => {
    const dateNodes = adfUtil.findDateNodes(valueCell);
    if (dateNodes.length < minDates) {
      if (minDates === 1) {
        if (minDates === 1) {
          this.addError(`Expected exactly 1 ${rowHeaderText}!`);
        } else {
          this.addError(`Expected at least 1 ${rowHeaderText}!`);
        }
      } else {
        this.addError(`Expected at least ${minDates} ${rowHeaderText}s!`);
      }
    } else {
      if (dateNodes.length > maxDates) {
        this.addError(`There are too many ${rowHeaderText}s!`);
      } else {
        checker(dateNodes, rowHeaderText);
      }
    }
  };

  statusChecker = (statusValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (statusNode: AdfNode, rowHeaderText: string) => {
      const statusText = adfUtil.getStatusText(statusNode);
      this.daciData.status = util.statusOf(statusText);
      if (util.isIn(statusText.toLowerCase().trim(), allowedStatuses)) {
        this.addPass(`Validated the status (${statusText})`);
      } else {
        this.addError(`The status is ${statusText}, but should be one of ${allowedStatuses.join(', ')}.`);
      }
      const statusTextLowersToColors = {
        'not started': 'neutral',
        'in progress': 'blue',
        'done': 'green',
        'obsolete': 'purple'
      };
      this.validateStatusNodeFormat(statusNode, 'status', statusTextLowersToColors);
    };
    this.checkStatusInCell(statusValueCellNode, rowHeaderText, checker);
  };

  impactChecker = (impactValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (impactNode: AdfNode, rowHeaderText: string) => {
      const impactText = adfUtil.getStatusText(impactNode);
      this.daciData.impact = impactText;
      if (util.isIn(impactText.toLowerCase().trim(), allowedImpacts)) {
        this.addPass(`Validated the impact (${impactText})`);
      } else {
        this.addError(`The impact is ${impactText}, but should be one of ${allowedImpacts.join(', ')}.`);
      }
      const impactTextLowersToColors = {
        'high': 'red',
        'medium': 'yellow',
        'low': 'green'
      };
      this.validateStatusNodeFormat(impactNode, 'impact', impactTextLowersToColors);
    };
    this.checkStatusInCell(impactValueCellNode, rowHeaderText, checker);
  };

  driverChecker = (driverValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (mentionNodes: AdfNode, rowHeaderText: string) => {
      const mentionNode = mentionNodes[0];
      const driverText = adfUtil.getMentionName(mentionNode);
      this.daciData.driver = driverText;
      this.addPass(`Validated there is a single driver (${driverText})`);
    };
    this.checkMentionsInCell(driverValueCellNode, rowHeaderText, 1, 1, checker);
  };

  dueDateChecker = (dueDateValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (dateNodes: AdfNode[], rowHeaderText: string) => {
      let dueDate: undefined | Date = undefined;
      const dateNode = dateNodes.length ? dateNodes[0] : undefined;
      if (dateNode) {
        dueDate = adfUtil.getDate(dateNode);
      }
      this.daciData.dueDate = dueDate;
      if (dueDate !== undefined) {
        this.daciData.millisecondsUntilDue = dueDate.getTime() - this.parameters.dateOfCheck.getTime();
        this.addPass(`Validated there is a due date (${dueDate.toString()})`);
        this.daciData.overdue = false;
        if (this.daciData.millisecondsUntilDue < 0) {
          if (this.daciData.status === Status.NotStarted || this.daciData.status === Status.InProgress) {
            this.daciData.overdue = true;
          }
        }

        if (this.daciData.overdue) {
          this.addError(`This decision is **overdue**.`);
        } else {
          let maximumDaysUntilDecisionDue = undefined;
          if (this.daciData.impact === Impact.Low) {
            maximumDaysUntilDecisionDue = this.parameters.maximumDaysUntilLowImpactDecisionDue;
          } else if (this.daciData.impact === Impact.Medium) {
            maximumDaysUntilDecisionDue = this.parameters.maximumDaysUntilMediumImpactDecisionDue;
          } else {
            maximumDaysUntilDecisionDue = this.parameters.maximumDaysUntilHighImpactDecisionDue;
          }
          const daysUntilDue = this.daciData.millisecondsUntilDue / (1000 * 60 * 60 * 24);
          if (daysUntilDue > maximumDaysUntilDecisionDue) {
            this.addError(`The due date is too distant. It should be less than ${maximumDaysUntilDecisionDue} days, but is currently ${Math.round(daysUntilDue)} days from now.`);
          }
        }
      } else {
        if (util.statusOf(this.daciData.status) !== Status.NotStarted) {
          this.addError(`There is no due date.`);
        }
      }
    };
    this.checkDatesInCell(dueDateValueCellNode, rowHeaderText, 0, 1, checker);
  };

  approverChecker = (approverValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (mentionNodes: AdfNode[], rowHeaderText: string) => {
      const mentionNode = mentionNodes[0];
      const approverText = adfUtil.getMentionName(mentionNode);
      this.daciData.approver = approverText;
      this.addPass(`Validated there is a single approver (${approverText})`);
    };
    this.checkMentionsInCell(approverValueCellNode, rowHeaderText, 1, 3, checker);
  };

  informedChecker = (informedValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (mentionNodes: AdfNode[], rowHeaderText: string) => {
      const informedText = adfUtil.getMentionNames(mentionNodes);
      this.daciData.informed = informedText;
      this.addPass(`Validated there is at least one person mentioned as informed: (${informedText})`);
    };
    this.checkMentionsInCell(informedValueCellNode, rowHeaderText, 1, 999999, checker);
  };

  contributorsChecker = (contributorsValueCellNode: AdfNode, rowHeaderText: string) => {
    const checker = (mentionNodes: AdfNode[], rowHeaderText: string) => {
      const contributorsText = adfUtil.getMentionNames(mentionNodes);
      this.daciData.contributors = contributorsText;
      this.addPass(`Validated there is at least one person mentioned as a contributor: (${contributorsText})`);
    };
    this.checkMentionsInCell(contributorsValueCellNode, rowHeaderText, 1, 999999, checker);
  };

  outcomeChecker = (outcomeValueCellNode: AdfNode, rowHeaderText: string) => {
    const outcomeText = adfUtil.getConcatenatedSectionText(outcomeValueCellNode);
    let outcomeErrorDetected = false;
    this.daciData.outcome = outcomeText;
    const requiresOutcome = this.daciData.status === Status.Done || this.daciData.status === Status.Obsolete;
    const outcomeTextForMessage = outcomeText ? outcomeText : '<absent>';
    if (outcomeText) {
      if (requiresOutcome) {
        this.addPass(`Validated that an outcome has been documented (${outcomeTextForMessage})`);
      } else {
        this.addError(`It's inappropriate to document an outcome (${outcomeTextForMessage}) since the status is not "${Status.Done}".`);
      }
    } else {
      if (requiresOutcome) {
        this.addError(`The outcome needs to be documented since the decision status is "${this.daciData.status}".`);
      }
    }
    if (!outcomeErrorDetected) {
      this.addPass(`Validated the presence/absence of the outcome (${outcomeTextForMessage}) as per the current status (${this.daciData.status})`);
    }
  };

  checkSummaryTableItem = (statusTableNode: AdfNode, rowHeaderText: string, checker: Function) => {
    const headerNodes = adfUtil.findTableHeadersWithText(statusTableNode, rowHeaderText);
    if (headerNodes.length === 1) {
      const headerNode = headerNodes[0];
      const statusHeaderPosition = adfUtil.getTableCellPosition(statusTableNode, headerNode);
      if (statusHeaderPosition) {
        const statusValuePosition: TableCellPosition = {
          columnIndex: statusHeaderPosition.columnIndex + 1,
          rowIndex: statusHeaderPosition.rowIndex
        };
        const valueCell = adfUtil.getCell(statusTableNode, statusValuePosition);
        if (valueCell) {
          checker(valueCell, rowHeaderText);
        } else {
          this.addError(`Could not find a ${rowHeaderText} value cell!`);
        }
      } else {
        this.addError(`Unable to find the position of header ${rowHeaderText}!`);
      }
    } else {
      this.addError(`Expected 1 header with text ${rowHeaderText}, but got ${headerNodes.length}!`);
    }
  };

  checkSectionPopulated = (adfNode: AdfNode, sectionText: string, linkCount: number, headerText: string, suffixText: undefined | string, minimumLength: number) => {
    const actualLength = sectionText.length + linkCount * this.parameters.linkTextLengthContribution;
    if (sectionText === undefined) {
      this.addError(`Unable to find a "${headerText}" section!`);
    } else if (actualLength < minimumLength) {
      this.addError(`The "${headerText}" section length of ${actualLength} is less than the minimum length requirement of ${minimumLength}. ${suffixText ? suffixText : ''}`);
    } else {
      this.addPass(`The "${headerText}" section length of ${actualLength} meets the minimum length requirement of ${minimumLength}.`);
    }
  };

  checkSummaryTableInfo = () => {
    const summaryTableNode = adfUtil.findTable(this.parameters.adf, ['Status', 'Impact', 'Approver']);
    if (summaryTableNode) {
      this.addPass('Validated there is a summary table');
      this.checkSummaryTableItem(summaryTableNode, 'Status', this.statusChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Impact', this.impactChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Driver', this.driverChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Due date', this.dueDateChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Approver', this.approverChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Informed', this.informedChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Contributors', this.contributorsChecker);
      this.checkSummaryTableItem(summaryTableNode, 'Outcome', this.outcomeChecker);
    } else {
      this.addError(`Unable to find the summary table!`);
    }
  };

  checkText = () => {
    this.daciData.backgroundText = adfUtil.getTextLogicallyWithinHeading(this.parameters.adf, 'Background').trim();
    this.daciData.backgroundLinkCount = adfUtil.countLinksLogicallyWithinHeading(this.parameters.adf, 'Background');
    this.checkSectionPopulated(
      this.parameters.adf,
      this.daciData.backgroundText,
      this.daciData.backgroundLinkCount,
      'Background',
      undefined,
      this.parameters.minimumBackgroundLength);
    this.daciData.relevantDataText = adfUtil.getTextLogicallyWithinHeading(this.parameters.adf, 'Relevant data').trim();
    this.daciData.relevantDataLinkCount = adfUtil.countLinksLogicallyWithinHeading(this.parameters.adf, 'Relevant data');
    this.checkSectionPopulated(
      this.parameters.adf,
      this.daciData.relevantDataText,
      this.daciData.relevantDataLinkCount,
      'Relevant data',
      'If this is irrelevant, perhaps enter "There is no data relevant to this decision - see the background section." into the relevant data section',
      this.parameters.minimumRelevantDataLength);
    if (this.parameters.minimumBackgroundPlusRelevantDataLength) {
      const backgroundLinkContributionLength = this.daciData.backgroundLinkCount * this.parameters.linkTextLengthContribution;
      const relevantDataLinkContributionLength = this.daciData.relevantDataLinkCount * this.parameters.linkTextLengthContribution;
      const backgroundLength = this.daciData.backgroundText ?
        this.daciData.backgroundText.length + backgroundLinkContributionLength : backgroundLinkContributionLength;
      const relevantDataLength = this.daciData.relevantDataText ?
        this.daciData.relevantDataText.length + relevantDataLinkContributionLength: relevantDataLinkContributionLength;
      const actualBackgroundPlusRelevantDataLength = backgroundLength + relevantDataLength;
      if (actualBackgroundPlusRelevantDataLength < this.parameters.minimumBackgroundPlusRelevantDataLength) {
        this.addError(`The background and relevant data sections should have a combined length of at least ${this.parameters.minimumBackgroundPlusRelevantDataLength} characters whereas their current total length is only ${actualBackgroundPlusRelevantDataLength} characters!`);
      }
    }
  };

  checkOptionDescriptions = (optionsTable: AdfNode, optionNumbersToData: any) => {
    const descriptionRow = adfUtil.findTableRowByHeaderColumnText(optionsTable, ['Description']);
    if (descriptionRow) {
      adfUtil.iterateRowCells(descriptionRow, (cellNode, columnIndex) => {
        if (columnIndex > 0) {
          const optionNumber = columnIndex;
          const optionDescription = adfUtil.getConcatenatedSectionText(cellNode);
          optionNumbersToData['option-' + optionNumber].description = optionDescription;
          if (optionDescription && optionDescription.length >= this.parameters.minimumOptionDescriptionLength) {
            this.addPass(`The description of option ${columnIndex} (${optionDescription}) meets the minimum length criteria (${this.parameters.minimumOptionDescriptionLength}).`);
          } else {
            this.addError(`The description of option ${columnIndex} (${optionDescription}) does not meet the minimum length criteria (${this.parameters.minimumOptionDescriptionLength})!`);
          }
        }
      });
    } else {
      this.addError(`The options table doesn't have a row with a header "Description"!`);
    }
  };

  checkProsAndConsCell = (cellNode: AdfNode, rowIndex: number, optionNumber: number, optionNumbersToData: any): void => {
    const emojiNodes = adfUtil.findEmojiNodes(cellNode);
    for (const emojiNode of emojiNodes) {
      const proConType = this.proConTypeFromEmoji(emojiNode);
      if (proConType) {
        const optionData = optionNumbersToData['option-' + optionNumber];
        const proConTypes = optionData.rowIndexesToProConTypes['row-' + rowIndex];
        proConTypes.push(proConType);
        optionData.proConsCount++;
      }
    }
  };

  proConTypeFromEmoji = (emojiNode: AdfNode): undefined | ProConType => {
    if (emojiNode.attrs.shortName === ':minus:') {
      return ProConType.Minus;
    } else if (emojiNode.attrs.shortName === ':plus:') {
      return ProConType.Plus;
    } else if (emojiNode.attrs.shortName === ':warning:') {
      return ProConType.Warning;
    } else if (emojiNode.attrs.shortName === ':question_mark:') {
      return ProConType.Question;
    } else {
      console.log('Unknown ProConType:', emojiNode.attrs.shortName);
      return undefined;
    }
  };

  checkOptionProsAndConsForRow = (prosAndConsRow: AdfNode, rowTitle: string, rowIndex: number, optionNumbersToData: any): void => {
    adfUtil.iterateRowCells(prosAndConsRow, (cellNode, columnIndex) => {
      if (columnIndex > 0) {
        const optionNumber = columnIndex;
        this.checkProsAndConsCell(cellNode, rowIndex, optionNumber, optionNumbersToData);
      }
    });

    // Compute the minimum and maximum numbers of pros and cons per cell.
    let minProConsInACell = undefined;
    let maxProConsInACell = undefined;
    adfUtil.iterateRowCells(prosAndConsRow, (cellNode, columnIndex) => {
      if (columnIndex > 0) {
        const optionNumber = columnIndex;
        const optionData = optionNumbersToData['option-' + optionNumber];
        const proConTypes = optionData.rowIndexesToProConTypes['row-' + rowIndex];
        const proConCount = proConTypes.length;
        if (minProConsInACell === undefined || minProConsInACell > proConCount) {
          minProConsInACell = proConCount;
        }
        if (maxProConsInACell === undefined || maxProConsInACell < proConCount) {
          maxProConsInACell = proConCount;
        }
      }
    });
    if (minProConsInACell > 0) {
      this.daciData.prosAndConsRowCount++;
      if (minProConsInACell !== maxProConsInACell) {
        this.addValidationResult(`For the "${rowTitle}" row, there are different numbers of pros and cons in different cells. This implies all considerations relevant to the decision have not been dealt with for each option. `, this.parameters.problemSeverityOfSingleProsConsRow);
      }
      if (maxProConsInACell > 1) {
        this.addValidationResult(`The are up to ${maxProConsInACell} pros and cons in a single option within the "${rowTitle}" row. It would be better to split this pros and cons row into separate rows representing each consideration relevant to this decision.`, this.parameters.problemSeverityOfSingleProsConsRow);
      }
    }
  };

  checkOptionProsAndCons = (optionsTable: AdfNode, optionsCount: number, optionNumbersToData: any): void => {
    adfUtil.iterateTableRows(optionsTable, (rowNode, rowIndex) => {
      const titleCell = rowNode.content[0];
      const rowTitle = adfUtil.getConcatenatedSectionText(titleCell);
      this.checkOptionProsAndConsForRow(rowNode, rowTitle, rowIndex, optionNumbersToData);
    });
    if (this.daciData.prosAndConsRowCount === 0) {
      this.addError(`No pros and cons could be found in the options table.`);
    }
  };

  checkOptionsTable = () => {
    const optionsConsideredNodes = adfUtil.getNodesLogicallyWithinHeading(this.parameters.adf, 'Options considered');
    const optionsTable = adfUtil.findFirstTableInNodes(optionsConsideredNodes);
    if (optionsTable) {
      const columnCount = adfUtil.countTableColumns(optionsTable);
      const optionsCount = columnCount - 1;
      if (optionsCount < this.parameters.minimumOptionsCount) {
        this.addValidationResult('At least two options need to be provided - the options table does not have enough columns representing options (the table has ${columnCount} columns).', this.parameters.problemSeverityOfSingleProsConsRow)
      }

      const rowCount = adfUtil.countTableRows(optionsTable);

      const optionNumbersToData = this.daciData.optionNumbersToData;
      for (let optionNumber = 1; optionNumber <= optionsCount; optionNumber++) {
        const optionData = {
          description: undefined,
          proConsCount: 0,
          rowIndexesToProConTypes: {}
        };
        for (let rowIndex = 0; rowIndex < rowCount; rowIndex++) {
          optionData.rowIndexesToProConTypes['row-' + rowIndex] = [];
        }
        optionNumbersToData['option-' + optionNumber] = optionData;
      }
      this.checkOptionDescriptions(optionsTable, optionNumbersToData);
      this.checkOptionProsAndCons(optionsTable, optionsCount, optionNumbersToData);

      for (let optionNumber = 1; optionNumber <= optionsCount; optionNumber++) {
        const optionData =  optionNumbersToData['option-' + optionNumber];
        // const proConTypes = optionData.rowIndexesToProConTypes['row-' + rowIndex];
        const proConsCount = optionData.proConsCount;
        if (proConsCount < this.parameters.minimumProsConsPerOption) {
          if (proConsCount === 0) {
            this.addError(`Option ${optionNumber} has no pros/cons.`);
          } else {
            this.addError(`Option ${optionNumber} has only ${proConsCount} pros/cons which is less than the minimum of ${this.parameters.minimumProsConsPerOption} that at least one other option has.`);
          }
        }

      }
    } else {
      this.addError(`Unable to find the options table. Expected it to be in a section titled "Options considered".`);
    }
  };

}

